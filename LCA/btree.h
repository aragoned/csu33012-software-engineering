#include <iostream>
#include <cstdlib>

using namespace std;

struct Node
{
  int val;
  Node *left;
  Node *right;

  Node(int newVal)
  {
    val = newVal;
  }
  
};

class btree
{
    public:
        btree(int val);
        ~btree();
 
        void insert(int val);
        void destroy_tree();
        Node* findLowestCommonAncestor(int p, int q);
        Node *root;
 
    private:
        void destroy_tree(Node *node);
        Node* insert(int val, Node *node);
        Node* findLowestCommonAncestor(int p, int q, Node *node);

};