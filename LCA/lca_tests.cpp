#include <gtest/gtest.h>
#include "btree.cpp"

TEST(LCATests, perfectTreeTests) 
{
    btree bst = btree(10);
    bst.insert(6);
    bst.insert(14);
    bst.insert(5);
    bst.insert(8);
    bst.insert(11);
    bst.insert(18);

    ASSERT_EQ( ((bst.findLowestCommonAncestor(11, 18))->val), 14 );
    ASSERT_EQ( ((bst.findLowestCommonAncestor(6, 8))->val), 6 );
    ASSERT_EQ( ((bst.findLowestCommonAncestor(8, 5))->val), 6 );
    ASSERT_EQ( ((bst.findLowestCommonAncestor(14, 11))->val), 14 );
}

int main(int argc, char **argv) 
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}