# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/c/users/dmara/LowestCommonAncestor/lca_tests.cpp" "/mnt/c/users/dmara/LowestCommonAncestor/build/CMakeFiles/executeTests.dir/lca_tests.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "_deps/googletest-src/googletest/include"
  "_deps/googletest-src/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/mnt/c/users/dmara/LowestCommonAncestor/build/_deps/googletest-build/googletest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/mnt/c/users/dmara/LowestCommonAncestor/build/_deps/googletest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
