#include "btree.h"

btree::btree(int val) {
  root = insert(val, NULL);
}

btree::~btree()
{
  destroy_tree();
}

void btree::destroy_tree() {
    destroy_tree(root);
}

void btree::destroy_tree(Node *node) {

  if (node !=NULL)
  {
    destroy_tree(node->left);
    destroy_tree(node->right);
    delete node;
  }
}

void btree::insert(int val) {
    insert(val, root);
}

Node* btree::insert(int val, Node *node) {
    if (node == NULL)
    {
        //Node* ptr = new Node(val);
        return new Node(val);
    }

    if (val < node->val)
        (node->left) = insert(val, node->left); 
    if (val > node->val)
        (node->right) = insert(val, node->right); 

    return node;
}

Node* btree::findLowestCommonAncestor(int p, int q) {
    return findLowestCommonAncestor(p, q, root);
}

Node* btree::findLowestCommonAncestor(int p, int q, Node *node) {
    if (node == NULL)
        return NULL;

    int nodeVal = node->val;
    if (nodeVal == p || nodeVal == q)
        return node; 

    Node* left_subtree = findLowestCommonAncestor(p, q, node->left);
    Node* right_subtree = findLowestCommonAncestor(p, q, node->right);

    if (left_subtree == NULL)
        return right_subtree;

    if (right_subtree == NULL)
        return left_subtree;
    
    return node; 
}
